{:title "About"
 :layout :page
 :page-index 0
 :navbar? true}

## About the author

My name is Arnaud. I'm a C++/Qt developer. I recently fell in love of functional programming and specially Clojure. I will use this blog to mainly document my learning of functional programming. I will try to explore FP through differents subjects like mazes, data structures and different projects. I may also talk a little bit of emacs, C++ and some other langs of paradigms.

## About this blog 

All the blog posts and static content of this blog are licenced under Creative Commons licence (using [these terms](https://creativecommons.org/licenses/by-sa/4.0/)). Feel free to use it like you want as long as you credit me.

All the code will be published in differents repository under my [framagit account](https://framagit.org/adelpeyroux). The source code will always be licensed under free licences (I use Gnu GPLv3 most of the time).
