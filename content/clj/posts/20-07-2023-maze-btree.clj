^{:nextjournal.clerk/visibility {:code :hide :result :hide}}
{:title "Maze - Binary Tree algorithm"
 :layout :post
 :date "2023-07-20"
 :tags ["clerk" "clojure" "maze"]}


^{:nextjournal.clerk/visibility {:code :hide :result :hide}}
(ns maze-btree
  (:require [nextjournal.clerk :as clerk]
            [clojure.string :as str])
  (:import (java.net.http HttpRequest HttpClient HttpResponse$BodyHandlers)
           (java.net URI)))


;; # Experiment with maze generation - Binary Tree algorithm
;;
;; ## Naive approach
;;
;; The Binary tree algorithm consists to start from a grid.
;; For each cell of the grid we choose randomly to "erase" a wall.
;; We will choose between two walls, the north one or the east one.
;;
;; First we will need some random function. The clojure.core/rand function seems to be a good candidate.
;; This function just retrun a random float between [0, 1[. We will wrap it into an other function that will return :north or :east.

(defn random-direction []
  (if (< (rand) 0.5)
    :north
    :east))

;; We can test it
(take 10 (repeatedly #(random-direction)))

;; We can now define a Cell. It will consist on an i and j coordinates and the direction of the sibling connected cell.
(defrecord Cell [col row linked-to])

;; We will also declare a function to build a grid. A grid consist of a array of cells.
(defn make-grid [rows cols]
  (for [j (range rows) i (range cols)]
    (Cell. i j
           (cond
             (zero? j) :east              ;; First row, we cant "erase" north wall.
             (= (dec cols) i) :north      ;; For the last collumn we can't "erase" the east wall.
             :else (random-direction))))) ;; For all the other cells we ramdomly "erase" a wall.

;; Now a little test
(make-grid 4 4)

;; It seems pretty good.
;; We will now create a maze type that will store the grid and its own size
(defrecord Maze [rows cols grid])

(defn make-maze [rows cols]
  (Maze. rows cols (make-grid rows cols)))

;; Now we will try to render this grid and then display the maze.
;; For this we will convert the grid to an svg string, and then we will use Clerk to display this svg.
;; First we need a function tat create the correct SVG path for a cell. When we draw a cell we just need to draw the remaining wall (north or east).
;; The south or west wall will be drawn from other cells or are the limits of the grid.
(defn cell-svg-path [w h cell]
  (let [x (* (:col cell) w)
        y (* (:row cell) h)]
    (if (= (:linked-to cell) :east)
      (str "M " x "," y " " (+ x w) "," y)
      (str "M " (+ x w) "," y " " (+ x w) "," (+ y h)))))

;; Now we have to draw the whole maze. For this we have to draw the grid limits using lines then all the cells using a path.
(defn display-maze [width height maze]
  (clerk/html [:svg {:width width :height height :stroke "black"}
               [:line {:x1 0 :y1 0 :x2 width :y2 0}]           ;; We need to draw the grid contour.
               [:line {:x1 width :y1 0 :x2 width :y2 height}]  ;; I choosed to do this line by line ...
               [:line {:x1 width :y1 height :x2 0 :y2 height}] ;; There is maybe a better way.
               [:line {:x1 0 :y1 height :x2 0 :y2 0}]
               (let [cell-width (quot width (:cols maze))
                     cell-height (quot height (:rows maze))]
                 [:path {:d (->>  (:grid maze)
                                  (map #(cell-svg-path cell-width cell-height %))
                                  (str/join " "))}])]))

;; Lets test it !
(display-maze 400 400 (make-maze 25 25))

;; ## Conclusion
;;
;; In this part I implemented the algorithm using a naive approach.
;; But, this implementation seems too simple. I would like to reimplement it using graphs, it may be more flexible and surely reusable (I hope 🤞).
